class InformationObject():
    #pass
    def __init__(s):
        s.cv_test = "Test OK!"
        
        #custom variables
        #s.sv_settings_file = 'settings.txt'
        #s.div_factor_1D = 16*2
        #s.div_factor_1D_y = s.div_factor_1D

        s.sv_settings_file = 'settings_MC1031.txt'
        ##s.sv_settings_file = 'settings_MC1037_on_ph1221.txt'
        #s.sv_settings_file = 'settings_MC1037.txt'
        ##s.sv_settings_file = 'settings_MC1037_eff.txt' #TEST
        #s.sv_settings_file = 'settings_MC1037_recovered_HMBC.txt' #recovered HMBC
        #s.sv_settings_file = 'settings_MC1051.txt'
        s.sv_settings_file = 'settings_MC1037_TMP.txt'
        s.div_factor_1D = 16*0.7
        s.div_factor_1D_y = s.div_factor_1D

        ### Full plot: ###
        s.cv_x_from = -.3#-0.5
        s.cv_x_to = 10.3 #10.5
        s.cv_y_from = -.3#-0.5
        s.cv_y_to = 10.3#10.5
        s.cv_separate_mult_value_for_fullPlot = 10
        s.cv_separate_mult_value_for_fullPlot = 10.0
        #s.cv_separate_mult_value_for_fullPlot = 9.01 #for MC1031_ROESY
        #s.cv_separate_mult_value_for_fullPlot = 9.00001 #for MC1031_ROESY
        #s.cv_separate_mult_value_for_fullPlot = 1.9 #for MC1031_ROESY
        s.cv_separate_mult_value_for_fullPlot = 15.
        s.axis_spec_x = 'PROTON'
        s.axis_spec_y = 'PROTON'
        s.cv_y_axis_rounding = 2 # Decimal positions

        s.cv_full_plot_size_size_x = 20
        s.cv_full_plot_size_size_y = 20
        s.cv_full_top_1D_height_1D_horiz = 3
        s.cv_full_top_1D_height_labels_horiz = 1
        s.cv_full_left_1D_height_1D_verti = 3
        s.cv_full_left_1D_height_labels_verti = 2

        ### grid plot: ###
        s.cv_center_line_width_grid2D = 0
        s.cv_border_line_width_grid2D = 3
        s.cv_center_line_width_grid_1D = 0
        s.cv_border_line_width_grid_1D = 3

        s.cv_grid_plot_size_size_x_and_y = 20
        s.cv_grid_plot_size_size_x = 20
        s.cv_grid_top_1D_height_1D_horiz = 3
        s.cv_grid_top_1D_height_labels_horiz = 1
        s.cv_grid_left_1D_height_1D_verti = 3
        s.cv_grid_left_1D_height_labels_verti = 2
        
        ### shared: ###
        s.cv_min_factor_1D_x = 0.3 #0.01
        s.cv_min_factor_1D_y = 0.3 #0.01

        ### web-page options: ###
        h_and_w = 450
        s.cv_web_plot_height = h_and_w
        s.cv_web_plot_width = h_and_w


    def init_after_input(s):
        s.check_hetro_spectrum()

        s.cv_div_factor_x = s.div_factor_1D
        s.cv_div_factor_y = s.div_factor_1D_y

        ### grid plot: ###
        s.cv_x_diameter_std = .3
        s.cv_y_diameter_std = s.cv_x_diameter_std
        s.cv_div_factor_x_1D_grid = s.div_factor_1D
        s.cv_div_factor_y_1D_grid = s.div_factor_1D_y

        
    def check_hetro_spectrum(s):
        if s.spec_type == 'HSQC' or s.spec_type == 'HMBC':
            #""" ### H-C values ###
            s.cv_separate_mult_value_for_fullPlot = 180#160
            s.cv_x_from = 0
            s.cv_x_to = 10
            s.cv_y_from = 0#-0.5
            s.cv_y_to = 200#10.5
            #s.cv_y_to = 150#10.5
            s.div_factor_1D_y = 100*3*2
            s.axis_spec_x = 'PROTON'
            s.axis_spec_y = 'CARBON'
            s.cv_y_axis_rounding = 1
            #"""

