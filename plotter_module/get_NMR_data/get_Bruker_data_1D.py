from plotter_module.data_containers.SpectrumObject_1D import SpectrumObject_1D
def get_Bruker_data_1D(spec_type, information_instance):
    spec_obj = SpectrumObject_1D(spec_type, information_instance)
    return spec_obj
